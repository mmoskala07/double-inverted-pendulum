%% Clear workspace
close all; clc; clearvars;

%% Import headers
params
model
controller

%% Choose controller (LQR of LQG)
choosen_controller = LQR;
if choosen_controller == LQG
    activate_KF = true;
else
    activate_KF = false;
end

%% Launch simulation
simTime = 10;
global simStep; simStep = Tp;
t = (0:simStep:simTime)';
disp('Simulation has started...');
sim('simulation_multibody.slx');
disp('Simulation finished');

%% Show animation
run animation;

%% Show results (plots,comparisons, etc.)
disp('Drawing results...');
run draws;