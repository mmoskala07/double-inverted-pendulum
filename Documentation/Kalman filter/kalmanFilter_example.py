import numpy as np
import matplotlib.pyplot as plt


class KalmanFilter:
    def __init__(self, A, B, C, initial_state_vector, process_noise_matrix, measurement_noise_matrix):
        self.A = A
        self.B = B
        self.C = C

        self.n = self.A.shape[0]

        self.X = initial_state_vector
        self.P = np.identity(self.n)

        self.Q = process_noise_matrix
        self.R = measurement_noise_matrix

    def predict(self, input):
        self.X = self.A.dot(self.X) + self.B.dot(input)
        self.P = self.A.dot(self.P).dot(self.A.transpose()) + self.Q

    def correct(self, measurement):
        Y = measurement - self.C.dot(self.X)
        S = self.C.dot(self.P).dot(self.C.transpose()) + self.R
        K = self.P.dot(self.C.transpose()).dot(np.linalg.inv(S))

        self.X = self.X + K.dot(Y)
        self.P = np.array(np.identity(self.n)-K.dot(self.C)).dot(self.P)

        return self.X


def voltage_measurement_example():
    # Measurements
    mean = 10
    std_dev = 0.2

    # Model
    # http://bilgin.esme.org/BitsAndBytes/KalmanFilterforDummies
    A = np.array([[1]])
    B = np.array([[0]])
    C = np.array([[1]])
    x0 = np.array([[mean]])
    Q = np.array([[0]])
    R = np.array([[std_dev**2]])

    # Create Kalman Filter
    kf = KalmanFilter(A, B, C, x0, Q, R)

    # Result memory
    measurements = []
    estimations = []
    P_matrix = []

    # Iterate over
    for i in range(500):
        measurement = np.random.normal(mean, std_dev)
        kf.predict(0)
        est = kf.correct(measurement)

        measurements.append(measurement)
        estimations.append(float(est))
        P_matrix.append(float(kf.P))

    # Show
    plt.subplot(2, 1, 1)
    plt.plot(measurements, label="Signal")
    plt.plot(estimations, label="Estimation")
    plt.grid(True)
    plt.legend(loc='upper right')
    plt.title("Reading voltage signal")

    plt.subplot(2, 1, 2)
    plt.plot(P_matrix, label="P matrix value")
    plt.grid(True)
    plt.legend(loc='upper right')
    plt.title("Uncertainty matrix value")
    plt.show()


def car_position_velocity_example():
    # Params
    dt = 0.1
    init_pos = 0
    init_vel = 2
    acceleration_std_dev = 0.1
    acceleration_mean = 0.0
    measurement_std_dev = 0.5

    # Model
    # http://bilgin.esme.org/BitsAndBytes/KalmanFilterforDummies
    A = np.array([[1, dt],
                  [0, 1]])
    B = np.array([[(dt**2)/2],
                  [dt]])
    C = np.identity(2)
    x0 = np.array([[init_pos],
                   [init_vel]])
    Q = B.dot(B.transpose()) * acceleration_std_dev**2
    R = np.identity(2) * measurement_std_dev**2

    # Create Kalman Filter
    kf = KalmanFilter(A, B, C, x0, Q, R)

    # Result memory
    meas_pos = []
    meas_vel = []
    est_pos = []
    est_vel = []

    # Iterate over
    for i in range(500):
        acceleration = np.random.normal(acceleration_mean, acceleration_std_dev)
        kf.predict(acceleration)

        if i == 0:
            measurement = np.array([[init_pos],
                                    [init_vel]])
        else:
            measurement = np.array([[init_pos + (i+1)*dt*init_vel + np.random.normal(0.0, measurement_std_dev)],
                                    [init_vel + np.random.normal(0.0, measurement_std_dev)]])
        est = kf.correct(measurement)

        meas_pos.append(float(measurement[0]))
        meas_vel.append(float(measurement[1]))
        est_pos.append(float(est[0]))
        est_vel.append(float(est[1]))

    plt.subplot(2, 1, 1)
    plt.plot(meas_pos, label="Signal")
    plt.plot(est_pos, label="Estimation")
    plt.grid(True)
    plt.ylabel("Position")
    plt.legend(loc='upper right')
    plt.title("Car position")

    plt.subplot(2, 1, 2)
    plt.plot(meas_vel, label="Signal")
    plt.plot(est_vel, label="Estimation")
    plt.grid(True)
    plt.ylabel("Velocity")
    plt.legend(loc='upper right')
    plt.title("Car velocity")
    plt.show()


if __name__ == "__main__":
    voltage_measurement_example()
    car_position_velocity_example()