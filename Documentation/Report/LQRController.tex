\section{LQR controller}
\label{cha:LQRController}

\textbf{LQR} stands for \textit{\textit{linear-quadratic regulator}}. The \textbf{LQ} problem appears when system dynamics are described by a set of \textit{linear differential equations} and the \textit{cost} is described by a \textit{quadratic} function. The LQR is an important part of the solution for the \textbf{LQG} (,,\textit{linear-quadratic-Gaussian}" regulator). The general purpose of LQR controller is to \textbf{minimalize a cost} function with \textit{weighting} factors. Code listing of Matlab script for LQR regulator's parameters is described in chapter \ref{sec:controller.m}.
\newline

\subsection{Matlab function - lqrd()}
There is a function \textbf{lqrd()}, which designs a \textit{discrete full-state-feedback regulator}. Equation ({\ref{eq:lqrd}}) calculates the discrete state-feedback.

\begin{equation}
	\label{eq:lqrd}
	K_d = lqrd(A,B,Q,R,T_s)
\end{equation}
where:\\
\begin{tabular}{l c l}
	$K_d$ & - & LQR regulator's gain\\
	$A$   & - & state (or system) matrix in state-space representation\\
	$B$   & - & input matrix in state-space representation\\
	$Q$   & - & matrix with weights for states $x$\\
	$R$   & - & matrix with weights for control $u$\\
	$T_s$ & - & sample time of the discrete regulator
\end{tabular}

The matrices $A$ and $B$ specify the continuous plant dynamics - equation (\ref{eq:plantDynamics}):
\begin{equation}
	\label{eq:plantDynamics}
	\dot{x} = Ax + Bu
\end{equation}

The LQR controller minimizes a discrete cost function equivalent to the continuous cost function - equation (\ref{eq:costFunction}):
\begin{equation}
	\label{eq:costFunction}
	J = \int_{0}^{\infty} \left( x^TQx + u^TRu \right) dt
\end{equation}

\subsubsection{$Q$ matrix}
$Q$ matrix contains weights for model's parameters on its diagonal. Weights were chosen for those model's parameters:
\begin{enumerate}
	\item cart position $cart\_pos$
	\item lower pendulum position $low\_pen\_pos$
	\item upper pendulum position $up\_pen\_pos$
	\item cart velocity $cart\_vel$
	\item lower pendulum velocity $low\_pen\_vel$
	\item upper pendulum velocity $up\_pen\_vel$
\end{enumerate}

$Q$ matrix looks like this:

\begin{equation}
	Q = \begin{bmatrix}
	cart_{pos} & 0 & 0 & 0 & 0 & 0\\
	0 & low\_pen_{pos} & 0 & 0 & 0 & 0\\
	0 & 0 & up\_pen_{pos} & 0 & 0 & 0\\
	0 & 0 & 0 & cart_{vel} & 0 & 0\\
	0 & 0 & 0 & 0 &  low\_pen_{vel} & 0\\
	0 & 0 & 0 & 0 & 0 & up\_pen_{vel}
	\end{bmatrix}
\end{equation}

\subsubsection{$R$ matrix}
$R$ matrix contains weights for control signal $u$. Our system has only one control signal, so matrix is a scalar:

\begin{equation}
	R = \begin{bmatrix}
		signal
	\end{bmatrix} = signal
\end{equation}

\subsubsection{Weights}
The higher weight is, the more ,,\textit{important}" a value is. Weight informs LQR regulator how much we want a value to stabilize as fast as possible. The higher weight is, the faster we want a value to stabilize.

\newpage
\subsection{Results of LQR controller}
There are two models of inverted double pendulum:
\begin{enumerate}
	\item State-space model in Simulink
	\item Model in Simulink Multibody
\end{enumerate}

Each pair of figures compare results of LQR controller for both models. Dimensions of guide, cart, lower and upper pendulum remain the same for all results presented below as follows:
\begin{enumerate}
	\item mass $m$
		\begin{itemize}
			\item $m_{cart} = 3$ kg
			\item $m_{lower \; pendulum} = 1$ kg
			\item $m_{upper \; pendulum} = 1.5$ kg
		\end{itemize}
	\item length
		\begin{itemize}
			\item $l_{guide} = 10$ m
			\item $l_{cart} = 1$ m
			\item $l_{lower \; pendulum} = 1$ m
			\item $l_{upper \; pendulum} = 1.5$ m
		\end{itemize}
	\item width
		\begin{itemize}
			\item $w_{guide} = 1$ m
			\item $w_{cart} = 1$ m
			\item $w_{lower \; pendulum} = 0.07$ m
			\item $w_{upper \; pendulum} = 0.07$ m
		\end{itemize}
	\item height
		\begin{itemize}
			\item $h_{guide} = 0.1$ m
			\item $h_{cart} = 0.2$ m
			\item $h_{lower \; pendulum} = 0.07$ m
			\item $h_{upper \; pendulum} = 0.07$ m
		\end{itemize}
\end{enumerate}

\newpage
\subsubsection{Test 1}
The figure {\ref{fig:controllerStateSpace1}} and \ref{fig:controllerMultibody1} presents the obtained results for those initial conditions:

\begin{enumerate}
	\item cart position $cart\_pos = 0$
	\item lower pendulum position $low\_pen\_pos = \frac{\pi}{6}$
	\item upper pendulum position $up\_pen\_pos = \frac{\pi}{12}$
	\item cart velocity $cart\_vel = 0$
	\item lower pendulum velocity $low\_pen\_vel = 0$
	\item upper pendulum velocity $up\_pen\_vel = 0$
\end{enumerate}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.68\textwidth]{Graphics/LQR_controller/controllerStateSpace1.png}\\
	\caption{Result of 1st test for state-space model.}
	\label{fig:controllerStateSpace1}
\end{figure}
\FloatBarrier

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.68\textwidth]{Graphics/LQR_controller/controllerMultibody1.png}\\
	\caption{Result of 1st test for multibody model.}
	\label{fig:controllerMultibody1}
\end{figure}
\FloatBarrier

Plots on figures \ref{fig:controllerStateSpace1} and \ref{fig:controllerMultibody1} have similar shape. All measurements (position, velocity and force) reach $0$ faster in multibody simulation.

\newpage
\subsubsection{Test 2}
The figure {\ref{fig:controllerStateSpace2}} and \ref{fig:controllerMultibody2} presents the obtained results for those initial conditions (the only changed variable from previous test is cart velocity):

\begin{enumerate}
	\item cart position $cart\_pos = 0$
	\item lower pendulum position $low\_pen\_pos = \frac{\pi}{6}$
	\item upper pendulum position $up\_pen\_pos = \frac{\pi}{12}$
	\item cart velocity $cart\_vel = 5$
	\item lower pendulum velocity $low\_pen\_vel = 0$
	\item upper pendulum velocity $up\_pen\_vel = 0$
\end{enumerate}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.63\textwidth]{Graphics/LQR_controller/controllerStateSpace2.png}\\
	\caption{Result of 2nd test for state-space model.}
	\label{fig:controllerStateSpace2}
\end{figure}
\FloatBarrier

\begin{figure}[h]
	\centering
	\includegraphics[width=0.63\textwidth]{Graphics/LQR_controller/controllerMultibody2.png}\\
	\caption{Result of 2nd test for multibody model.}
	\label{fig:controllerMultibody2}
\end{figure}
\FloatBarrier

Cart measurements (its position and velocity) of two models are mirror reflection to each other (because of $-1$ gain blocks). Plots on figures \ref{fig:controllerStateSpace2} and \ref{fig:controllerMultibody2} have similar shape.

\subsubsection{Test 3}
The figure {\ref{fig:controllerStateSpace3}} and \ref{fig:controllerMultibody3} presents the obtained results for those initial conditions (variables changed from previous test: cart velocity, lower and upper pendulum position):

\begin{enumerate}
	\item cart position $cart\_pos = 0$
	\item lower pendulum position $low\_pen\_pos = \frac{\pi}{5}$
	\item upper pendulum position $up\_pen\_pos = \frac{\pi}{7}$
	\item cart velocity $cart\_vel = 0$
	\item lower pendulum velocity $low\_pen\_vel = 0$
	\item upper pendulum velocity $up\_pen\_vel = 0$
\end{enumerate}

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Graphics/LQR_controller/controllerStateSpace3.png}\\
	\caption{Result of 3rd test for state-space model.}
	\label{fig:controllerStateSpace3}
\end{figure}
\FloatBarrier

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Graphics/LQR_controller/controllerMultibody3.png}\\
	\caption{Result of 3rd test for multibody model.}
	\label{fig:controllerMultibody3}
\end{figure}
\FloatBarrier

Plots on figures \ref{fig:controllerStateSpace3} and \ref{fig:controllerMultibody3} have similar shape.