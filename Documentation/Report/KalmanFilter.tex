\section{Kalman Filter}
\label{sec:KalmanFilter}

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm,height=14cm,keepaspectratio]{../KalmanFilter.PNG}
	\caption{Kalman Filter visualization.}
	\label{fig:KalmanFilter}
\end{figure}
\FloatBarrier

The Kalman Filter is used to estimate the true value returned from a noisy signal faster than other methods (for instance the average) would. It calculates a linear minimum variance of the error over time-carrying and time-invariant filter. It's the best linear filter in the class of linear filters.
\newline

\subsection{Introduction}
\label{subsec:Introduction}

At the beginning it can be described with a simplified equation:

\begin{equation}
\hat{x}_{k+1} = (1 - K) x_k + K y_k
\end{equation}

The purpose is to find $\hat{x}_{k+1}$ - the estimate of signal $x$.
The only unknown component in this equation is the $K$ (Kalman gain). It is essential to calculate $K$ for each consequent state which is not an easy task. Kalman gain says how new incoming data have to be considered and its value is in range (0,1).
Let the $K$ be assumed to be equal to $0.5$. With such a value the Kalman filter is a simple averaging one - some "smarter" $K$ coefficient should be found. Kalman filter finds the most optimum averaging factor for each consequent state as well as "remembers" some information about past states.
\newline

\subsection{Mathematical description}
\label{subsec:MathematicalDescription}

\subsubsection{Model's state space equations (continuous)}
\begin{equation}
\dot x = Ax + Bu
\end{equation}
$$ y = Cx + Du $$

\subsubsection{Model's state space equations (discrete)}
\begin{equation}
x_{k+1} = A_d x_k + B_d u_k
\end{equation}
$$ y_k = C_d x_k + D_d u_k $$

\subsubsection{Kalman filter's equations}
\begin{equation}
x_{k+1} = A_d x_k + B_d u_k + w_k
\end{equation}
$$ y_k = C_d x_k + v_k $$
where $w_k$ is the process noise and $v_k$ is the measurement noise.
\newline

State vector is formed almost as a state space but additionally there is the process noise that has an impact on the object. Measurements are never perfect thus an additional uncertainty is added (measurement noise).
\newline 
\subsubsection{Kalman filter's algorithm}
\begin{enumerate}
	\item Time update (prediction)
	\begin{equation}
	\hat{x}_k = A_d x_{k-1} + B_d u_k
	\end{equation}
	$$ P_k = A_d +P_{k-1} A_d^T + Q $$
	\item Measurement Update (correction)
	\begin{equation}
	E = y_k - C_d \hat x_k
	\end{equation}
	$$ S = C_d P_k C_d^T + R $$
	$$ K = P_k C_d^T S^{-1} $$
	$$ \hat{x}_k = \hat x_k + KE $$
	$$ P_k = (I - K C_d) P_k $$
\end{enumerate}

\subsubsection{Variables description}
\begin{itemize}
	\item $A_d$ - discrete time state transition matrix
	\item $B_d$ - discrete time input matrix
	\item $C_d$ - discrete time measurement matrix
	\item $x$   - state vector
	\item $u$   - input
	\item $P$   - uncertainty matrix
	\item $Q$   - process noise matrix
	\item $E$   - error matrix between measurement and prediction
	\item $R$   - measurement noise matrix
	\item $K$   - Kalman gain
	\item $I$   - identity matrix
\end{itemize}

\subsubsection{Initial state}

If there is no information about the initial state of the system an assumption is taken that $X_{k-1}$ is equal to $0$. This assumption can slightly affect the estimation at the beginning. 

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm,height=14cm,keepaspectratio]{../Kalman filter/KF_initial_state.PNG}
	\caption{Initial stare of the Kalman filter.}
	\label{fig:KF_initial_state}
\end{figure}
\FloatBarrier

\subsubsection{Noise matrices}

Simplifying the problem it might be said noise matrices are the amount of the noise in the system. The diagonals of the matrices indicates the variance of each state variable. Off diagonals contain the covariances between the different state variables (e.g. velocity of the cart vs velocity of the pendulum).

\begin{equation}
x_{k+1} = A_d x_k + B_d u_k + w_k
\end{equation} 
$$ w_k = B_d U_{k~external} $$
$$ Q = B_d \cdot B_d^T \cdot S^2_{u~external} ~~~_{(6x6)} $$

$B_d$ matrix describes how external force affects the system. The more massive the cart or the pendulum is, the less influence on the system has the noise $S^2_{u~external}$. It is worth to notice that not all variables in the state space are affected by the $S^2_{u~external}$ noise (which is a result of the multiplication $B_d \cdot B_d$).
\newline

\subsection{Kalman Filter in double inverted pendulum system}

The LQG controller uses the Kalman Filter as a signal estimator. The Kalman Filter allows to estimate the signal based on the previous data and the known process and measurement noise matrices.
In comparison to LQR controller the LQG gives much smoother force signal which affects actuator and its utilization significantly.

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm,height=14cm,keepaspectratio]{../Kalman filter/KF_double_inverted_pendulum.PNG}
	\caption{Signals before and after the Kalman Filter block.}
	\label{fig:KF_double_inverted_pendulum}
\end{figure}
\FloatBarrier

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm,height=14cm,keepaspectratio]{../Kalman filter/LQG_state_space.PNG}
	\caption{Linearized model output.}
	\label{fig:LQG_state_space}
\end{figure}
\FloatBarrier

If we use the same Kalman Filter block but with multibody model then we can observe an interesting behavior.

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm,height=14cm,keepaspectratio]{../Kalman filter/KF_double_inverted_pendulum_multibody.PNG}
	\caption{Signals before and after the Kalman Filter block.}
	\label{fig:KF_double_inverted_pendulum_multibody}
\end{figure}
\FloatBarrier

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm,height=14cm,keepaspectratio]{../Kalman filter/LQG_multibody.PNG}
	\caption{Linearized model output.}
	\label{fig:LQG_multibody}
\end{figure}
\FloatBarrier

It can be seen that the Kalman Filter's estimation is far away from the real data. The Kalman Filter estimates are good enough for cart position; the lower pendulum estimation is acceptable; however, the upper pendulum estimation is a complete disaster.
The reason why the Kalman Filter does not work well with the multibody model are the nonlinearities. Kalman Filter predicts a signal basing on a linearized model and corrects it basing on measurements.
The multibody model is a non-linear one so in order to achieve correct estimations an extended Kalman Filter should be used. In such case however, a "dancing" double pendulum would be received - especially if the noise in the system would be increased. The system would then get away from the region of stability.
\newline


\subsection{An example of Kalman Filter's operation}
\label{subsec:EeOfKalman}

This example is written in python. It generates a random set of voltage measurements with some noise~(\ref{lst:VoltMeasurement}). Then it applies the Kalman Filter~(\ref{lst:KalmanFilter}). It also generates a chart with the comparison between filtered and unfiltered measurements~(\ref{fig:FilteredUnfilteredMeasurements}).

\subsubsection{Kalman Filter}
\begin{lstlisting}[language=python, name=KalmanFilter, numbers=left, label={lst:KalmanFilter}]
class KalmanFilter:
    def __init__(self, A, B, C, initial_state_vector, process_noise_matrix, measurement_noise_matrix):
        self.A = A
        self.B = B
        self.C = C

        self.n = self.A.shape[0]

        self.X = initial_state_vector
        self.P = np.identity(self.n)

        self.Q = process_noise_matrix
        self.R = measurement_noise_matrix

    def predict(self, input):
        self.X = self.A.dot(self.X) + self.B.dot(input)
        self.P = self.A.dot(self.P).dot(self.A.transpose()) + self.Q

    def correct(self, measurement):
        Y = measurement - self.C.dot(self.X)
        S = self.C.dot(self.P).dot(self.C.transpose()) + self.R
        K = self.P.dot(self.C.transpose()).dot(np.linalg.inv(S))

        self.X = self.X + K.dot(Y)
        self.P = np.array(np.identity(self.n)-K.dot(self.C)).dot(self.P)

        return self.X
\end{lstlisting}

\subsubsection{A random set of voltage measurements with some noise}
\begin{lstlisting}[language=python, name='', numbers=left, label={lst:VoltMeasurement}]
def voltage_measurement_example():
    # Measurements
    mean = 10
    std_dev = 0.2

    # Model
    # http://bilgin.esme.org/BitsAndBytes/KalmanFilterforDummies
    A = np.array([[1]])
    B = np.array([[0]])
    C = np.array([[1]])
    x0 = np.array([[mean]])
    Q = np.array([[0]])
    R = np.array([[std_dev**2]])

    # Create Kalman Filter
    kf = KalmanFilter(A, B, C, x0, Q, R)

    # Result memory
    measurements = []
    estimations = []
    P_matrix = []

    # Iterate over
    for i in range(500):
        measurement = np.random.normal(mean, std_dev)
        kf.predict(0)
        est = kf.correct(measurement)

        measurements.append(measurement)
        estimations.append(float(est))
        P_matrix.append(float(kf.P))

    # Show
    plt.subplot(2, 1, 1)
    plt.plot(measurements, label="Signal")
    plt.plot(estimations, label="Estimation")
    plt.grid(True)
    plt.legend(loc='upper right')
    plt.title("Reading voltage signal")

    plt.subplot(2, 1, 2)
    plt.plot(P_matrix, label="P matrix value")
    plt.grid(True)
    plt.legend(loc='upper right')
    plt.title("Uncertainty matrix value")
    plt.show()
\end{lstlisting}


\begin{figure}[h]
	\centering
	\includegraphics[width=15cm,height=14cm,keepaspectratio]{../Kalman filter/ReadingVoltageExample.PNG}
	\caption{Comparison between filtered and unfiltered measurements.}
	\label{fig:FilteredUnfilteredMeasurements}
\end{figure}
\FloatBarrier

\subsubsection{An example with a riding car}
\begin{lstlisting}[language=python, name='', numbers=left, label={lst:RidingCarExample}]
def car_position_velocity_example():
    # Params
    dt = 0.1
    init_pos = 0
    init_vel = 2
    acceleration_std_dev = 0.1
    acceleration_mean = 0.0
    measurement_std_dev = 0.5

    # Model
    # http://bilgin.esme.org/BitsAndBytes/KalmanFilterforDummies
    A = np.array([[1, dt],
                  [0, 1]])
    B = np.array([[(dt**2)/2],
                  [dt]])
    C = np.identity(2)
    x0 = np.array([[init_pos],
                   [init_vel]])
    Q = B.dot(B.transpose()) * acceleration_std_dev**2
    R = np.identity(2) * measurement_std_dev**2

    # Create Kalman Filter
    kf = KalmanFilter(A, B, C, x0, Q, R)

    # Result memory
    meas_pos = []
    meas_vel = []
    est_pos = []
    est_vel = []

    # Iterate over
    for i in range(500):
        acceleration = np.random.normal(acceleration_mean, acceleration_std_dev)
        kf.predict(acceleration)

        if i == 0:
            measurement = np.array([[init_pos],
                                    [init_vel]])
        else:
            measurement = np.array([[init_pos + (i+1)*dt*init_vel + np.random.normal(0.0, measurement_std_dev)],
                                    [init_vel + np.random.normal(0.0, measurement_std_dev)]])
        est = kf.correct(measurement)

        meas_pos.append(float(measurement[0]))
        meas_vel.append(float(measurement[1]))
        est_pos.append(float(est[0]))
        est_vel.append(float(est[1]))

    plt.subplot(2, 1, 1)
    plt.plot(meas_pos, label="Signal")
    plt.plot(est_pos, label="Estimation")
    plt.grid(True)
    plt.ylabel("Position")
    plt.legend(loc='upper right')
    plt.title("Car position")

    plt.subplot(2, 1, 2)
    plt.plot(meas_vel, label="Signal")
    plt.plot(est_vel, label="Estimation")
    plt.grid(True)
    plt.ylabel("Velocity")
    plt.legend(loc='upper right')
    plt.title("Car velocity")
    plt.show()
\end{lstlisting}

\begin{figure}[h]
	\centering
	\includegraphics[width=15cm,height=14cm,keepaspectratio]{../Kalman filter/RidingcarExample.PNG}
	\caption{Comparison between velocity and position signals and their estimation.}
	\label{fig:RidingCarExampleFig}
\end{figure}
\FloatBarrier


