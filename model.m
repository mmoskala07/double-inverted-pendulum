%% Import headers
params

%% System equations after linearization
d1 = m_cart + m_pend_low + m_pend_upp;
d2 = (m_pend_low/2 + m_pend_upp)*l_pend_low;
d3 = (m_pend_upp*l_pend_upp)/2;
d4 = (m_pend_low/3 + m_pend_upp)*l_pend_low^2;
d5 = (m_pend_upp*l_pend_low*l_pend_upp)/2;
d6 = (m_pend_upp*l_pend_upp^2)/3;
D_thetas = [d1, d2, d3;
            d2, d4, d5;
            d3, d5, d6];

C_thetas = diag([b, 0, 0]);

g2 = -(m_pend_low/2 + m_pend_upp)*l_pend_low*g;
g3 = -(m_pend_upp*l_pend_upp*g)/2;
dG_thetas = diag([0, g2, g3]);

H = [1;0;0];

%% Continous state matrices
A = [zeros(3),                              eye(3);
     -inv(D_thetas)*(C_thetas + dG_thetas), zeros(3)];
B = [zeros(3,1);
     inv(D_thetas)*H];
C = eye(6);
D = zeros(6,1);

%% Discrete state matrices
ss_discr = c2d(ss(A, B, C, D), Tp);
[Ad, Bd, Cd, Dd] = ssdata(ss_discr);

%% Initial state vector

init_cart_pos = 0;
init_cart_vel = 0;
init_pendulum_lower_pos = pi / 12;
init_pendulum_lower_vel = 0;
init_pendulum_upper_pos = pi / 6;
init_pendulum_upper_vel = 0;

init_state = [init_cart_pos;
              init_pendulum_lower_pos;
              init_pendulum_upper_pos;
              init_cart_vel;
              init_pendulum_lower_vel;
              init_pendulum_upper_vel]; 
