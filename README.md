# Double Inverted Pendulum

![DoubleInvertedPendulumImage](Documentation/DoubleInvertedPendulum.png)

## Building
Linux:
~~~{.sh}
git clone https://gitlab.com/mmoskala07/double-inverted-pendulum.git
~~~
Windows:
~~~{.sh}
https://gitlab.com/mmoskala07/double-inverted-pendulum/-/archive/master/inverted_pendulum-master.zip
~~~

## Executing
In Matlab run file `launcher.m`

## Paper
[Double Inverted Pendulum](Documentation/DoubleInvertedPendulum.pdf)

## Videos
Inside videos showing how different controllers cope with problem
![Linear Quadratic Gaussian Controller](Documentation/Video/LQG_linearized.mp4)
![Linear Quadratic Regression](Documentation/Video/LQR_linearized.mp4)

## Maintainer
Mateusz Moskała  
mmoskala07@gmail.com  

Monika Król  
Wojciech Grzeliński  
Bartosz Jaśko  
