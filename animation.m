global c; % obiekt wozka
global animIter; % aktualna iteracja animacji

%% wyznaczenie kroku animacji na podstawie wynikow symulacji
% s = test_system_result(:,1); % wektor polozenia wozka
% theta_lower = test_system_result(:,2) * 180 / pi; % theta pendulum lower
% theta_upper = test_system_result(:,3) * 180 / pi; % theta pendulum upper

% s = before_KF(:,1); % wektor polozenia wozka
% theta_lower = before_KF(:,2) * 180 / pi; % theta pendulum lower
% theta_upper = before_KF(:,3) * 180 / pi; % theta pendulum upper

s = after_KF(:,1); % wektor polozenia wozka
theta_lower = after_KF(:,2) * 180 / pi; % theta pendulum lower
theta_upper = after_KF(:,3) * 180 / pi; % theta pendulum upper

% s = system_result(:,1); % wektor polozenia wozka
% theta_lower = system_result(:,2) * 180 / pi; % theta pendulum lower
% theta_upper = system_result(:,3) * 180 / pi; % theta pendulum upper

if (simStep < 0.03)
    skip = 1:(round(0.03/simStep)):length(t);
    s = s(skip);
    theta_lower = theta_lower(skip);
    theta_upper = theta_upper(skip);
    animStep = 0.03;
else
    animStep = simStep;
end

%% rozpoczecie malowania

f = figure('doublebuffer','on','units','normalized','outerposition',[0 0 1 1]);
figMargin = 0.05;
cartWidth = 0.2;
axes('position', [figMargin figMargin 1-2*figMargin 1-2*figMargin]); axis equal;
standLength = 5;

% dopasowanie osi x do wynikow symulacji i parametrow malowania
minX = -1;
maxX = 1;

if (max(s) > maxX)
    maxX = max(s);
end
if (min(s) < minX)
    minX = min(s);
end

yAxis = [-1 3]; % dopasowanie osi y do wysokosci wahadla
xAxis = [minX - cartWidth/2 - 0.5,maxX + cartWidth/2 + 0.5];
patch([xAxis(1),xAxis(1),xAxis(2),xAxis(2)],[-1,1,1,-1]*0.01,'k'); % malowanie mocowania

c = pendulum_cart_model(cartWidth); % inicjalizacja wozka
set(gca,'XLim',xAxis,'YLim',yAxis);grid on;

%% parametry timera odpowiadajacego za uruchomienie animacji
tim = timer('ExecutionMode','fixedRate','Period',animStep,'TasksToExecute',length(t));
tim.StartFcn = {@TimerStartEnd, 'Animation started...'};
tim.TimerFcn = {@TimerFcn,s,theta_lower,theta_upper};
tim.StopFcn = {@TimerStartEnd, 'Animation finished'};

%% start animacji
tic();
pause(3);
start(tim); % uruchomienie animacji
wait(tim); % zablokowanie wykonania kodu do czasu zakonczenia wykonywania kodu w timerze
toc();
delete(tim);