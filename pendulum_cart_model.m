classdef pendulum_cart_model
    properties(Access = public)
        cartWidth; % dlugosc
        cartHeight; % wysokosc
        lowerPendulumLength; % dlugosc wahadla
        upperPendulumLength; % dlugosc wahadla
        pendulumThickness; % grubosc wahadla
        bigRadius = 0.05; % promien duzego okregu mocowania
        smallRadius = 0.025; % promien malego okregu mocowania
        
        patches = [];
        cartAxis;
        
        upper_rotate_origin;
        
        lastX = 0; % poprzednia pozycja wozka
        lastTheta_lower = 0; 
        lastTheta_upper = 0;
    end
    methods(Access = public)
        % konstrutor inicjalizujacy wszystkie parametry wozka
        function obj = pendulum_cart_model(width)
            obj.cartWidth = width;
            obj.cartHeight = width / 2;
            obj.lowerPendulumLength = 5 * width;
            obj.upperPendulumLength = 7.5 * width;
            obj.pendulumThickness = width / 10;
            obj.bigRadius = width / 4;
            obj.smallRadius = width / 8;
            
            x = [-obj.cartWidth/2,-obj.cartWidth/2,obj.cartWidth/2,obj.cartWidth/2];
            y = [-obj.cartHeight/2,obj.cartHeight/2,obj.cartHeight/2,-obj.cartHeight/2];
            obj.patches = [obj.patches,patch(x,y,'r')];
            t = 0:pi/10:2*pi;
            st = sin(t);
            ct = cos(t);
            x = obj.bigRadius * st;
            y = obj.bigRadius * ct + obj.cartHeight/2;
            obj.patches = [obj.patches,patch(x,y,'y')];
            x = [-obj.pendulumThickness/2,obj.pendulumThickness/2,obj.pendulumThickness/2,-obj.pendulumThickness/2];
            y = [obj.cartHeight/2,obj.cartHeight/2,obj.cartHeight/2+obj.lowerPendulumLength,obj.cartHeight/2+obj.lowerPendulumLength];
            obj.patches = [obj.patches,patch(x,y,'b')];
            x = obj.smallRadius * st;
            y = obj.smallRadius * ct + obj.cartHeight/2;
            obj.patches = [obj.patches,patch(x,y,'y')];
            
            % upper pendulum
            temp = obj.cartHeight/2+obj.lowerPendulumLength+obj.upperPendulumLength;
            x = [-obj.pendulumThickness/2,obj.pendulumThickness/2,obj.pendulumThickness/2,-obj.pendulumThickness/2];
            y = [obj.cartHeight/2+obj.lowerPendulumLength,obj.cartHeight/2+obj.lowerPendulumLength,temp,temp];
            obj.patches = [obj.patches,patch(x,y,'g')];
            
            %pendulums joint
            x = obj.smallRadius * st;
            y = obj.smallRadius * ct + obj.cartHeight/2+obj.lowerPendulumLength;
            obj.patches = [obj.patches,patch(x,y,'y')];
            
            obj.cartAxis = [0,obj.cartHeight/2,0];
            obj.upper_rotate_origin = [0,obj.cartHeight/2+obj.lowerPendulumLength,0];
        end
        % funkcja przesuwajaca wozek do zadanej pozycji x
        function obj = move(obj,x)
            delta = x - obj.lastX;
            for i = 1:numel(obj.patches)
                newPos = get(obj.patches(i),'XData') + delta;
                set(obj.patches(i),'XData',newPos);
            end
            obj.cartAxis(1) = obj.cartAxis(1) + delta;
            obj.upper_rotate_origin(1) = obj.upper_rotate_origin(1) + delta;
            obj.lastX = x;
        end
        % funkcja obracajaca wahadla do zadanego kata theta
        function obj = rotate(obj,theta_lower,theta_upper)
            delta_lower = theta_lower - obj.lastTheta_lower;
            delta_upper = theta_upper - obj.lastTheta_upper;
            
            delta_x = (obj.lowerPendulumLength*sind(theta_lower)) - (obj.lowerPendulumLength*sind(obj.lastTheta_lower));
            delta_y = (obj.lowerPendulumLength*cosd(theta_lower)) - (obj.lowerPendulumLength*cosd(obj.lastTheta_lower));
            
            for i = 5:6
                newPos_upper_X = get(obj.patches(i),'XData') + delta_x;
                newPos_upper_Y = get(obj.patches(i),'YData') + delta_y;
                set(obj.patches(i),'XData',newPos_upper_X);
                set(obj.patches(i),'YData',newPos_upper_Y);
            end
            
            obj.upper_rotate_origin(1) = obj.upper_rotate_origin(1) + delta_x;
            obj.upper_rotate_origin(2) = obj.upper_rotate_origin(2) + delta_y;
            
            rotate(obj.patches(3),[0 0 -1],delta_lower,obj.cartAxis);
            rotate(obj.patches(5),[0 0 -1],delta_upper,obj.upper_rotate_origin);
            
            obj.lastTheta_lower = theta_lower;
            obj.lastTheta_upper = theta_upper;
        end
    end
end