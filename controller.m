%% LQR controller parameters

%% Weights

% Cart
w_cart_pos = 1;
w_cart_vel = 50;

% Lower pendulum
w_pendulum_lower_pos = 10;
w_pendulum_lower_vel = 100;

% Upper pendulum
w_pendulum_upper_pos = 20;
w_pendulum_upper_vel = 200;

% Control signal
w_signal = 0.1;

%% Matrices

% Matrix with weights for states x
Q = diag([w_cart_pos, w_pendulum_lower_pos, w_pendulum_upper_pos, ...
          w_cart_vel, w_pendulum_lower_vel, w_pendulum_upper_vel]);

% Matrix with weights for control u
R = w_signal;

%% LQR gain

% Discrete full-state-feedback regulator
K_LQR = lqrd(A,B,Q,R,Tp);