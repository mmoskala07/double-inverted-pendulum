%% Model parameters
m_cart = 3; % cart mass [kg]
m_pend_low = 1; % lower pendulum mass [kg]
m_pend_upp = 1.5; % upper pendulum mass [kg]
l_pend_low = 1; % upper pendulum length [m]
l_pend_upp = 1.5; % upper pendulum length [m]

%% Multibody - dimensions (brick solid)

l_guide = 10; % guide length [m]
w_guide = 1; % guide width [m]
h_guide = 0.1; % guide height [m]

l_cart = w_guide; % cart length [m]
w_cart = w_guide; % cart width [m]
h_cart = h_guide*2; % cart height [m]

l_low_pen = 0.07; % lower pendulum length [m]
w_low_pen = 0.07; % lower pendulum width [m]
h_low_pen = l_pend_low; % lower pendulum height [m]

l_upp_pen = l_low_pen; % upper pendulum length [m]
w_upp_pen = w_low_pen; % upper pendulum width [m]
h_upp_pen = l_pend_upp; % upper pendulum height [m]

%% Multibody - calculates brick solid's volume

v_cart = l_cart * w_cart * h_cart; % cart volume [m^3]
v_low_pen = l_low_pen * w_low_pen * h_low_pen; % lower pendulum volume [m^3]
v_upp_pen = l_upp_pen * w_upp_pen * h_upp_pen; % upper pendulum volume [m^3]

%% Multibody - density based on solid's volume

% Density of guide doesn't matter in our model
d_guide = 1000; % guide denstity [kg/m^3]
d_cart = m_cart/v_cart; % cart denstity [kg/m^3]
d_low_pen = m_pend_low/v_low_pen; % lower pendulum denstity [kg/m^3]
d_upp_pen = m_pend_upp/v_upp_pen; % upper pendulum denstity [kg/m^3]

%% Global parameters
g = 9.80665; % Earth acceleration
b = 0.1; % Moving friction

%% Sample time
Tp = 0.01; % s

%% Noises
var_measurement_noise = 0.01^2; % variance of measurement noise
var_process_noise = 0.001^2; % variance of process noise

%% Controller
LQR = "LQR";
LQG = "LQG";
