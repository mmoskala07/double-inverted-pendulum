%% Test system result
figure('NumberTitle', 'on', 'Name', 'Double Inverted Pendulum result','units','normalized','outerposition',[0 0 1 1]);
subplot(3, 1, 1);
plot(t, system_result(:,1)); hold on;
plot(t, system_result(:,2)); hold on;
plot(t, system_result(:,3)); hold on;
grid on;
legend('cart [m]', 'pendulum lower [rad]', 'pendulum upper [rad]');
xlabel('Time [s]');
ylabel('Position');
title(sprintf("Result of %s controller", choosen_controller));
subplot(3, 1, 2);
plot(t, system_result(:,4)); hold on;
plot(t, system_result(:,5)); hold on;
plot(t, system_result(:,6)); hold on;
grid on;
legend('cart [m/s]', 'pendulum lower [rad/s]', 'pendulum upper [rad/s]');
xlabel('Time [s]');
ylabel('Velocity');
subplot(3, 1, 3);
plot(t, force);
grid on;
xlabel('Time [s]');
ylabel('Force [N]');

if choosen_controller == LQG
    figure('NumberTitle', 'on', 'Name', 'Kalman Filter result', 'units','normalized','outerposition',[0 0 1 1]);
    subplot(3, 2, 1);
    plot(t, before_KF(:,1)); hold on;
    plot(t, after_KF(:,1)); grid on;
    xlabel('Time [s]');
    ylabel('Cart position [m]');
    subplot(3, 2, 2);
    plot(t, before_KF(:,4)); hold on;
    plot(t, after_KF(:,4)); grid on;
    xlabel('Time [s]');
    ylabel('Cart velocity [m/s]');
    subplot(3, 2, 3);
    plot(t, before_KF(:,2)); hold on;
    plot(t, after_KF(:,2)); grid on;
    xlabel('Time [s]');
    ylabel('Lower pendulum [rad]');
    subplot(3, 2, 4);
    plot(t, before_KF(:,5)); hold on;
    plot(t, after_KF(:,5)); grid on;
    xlabel('Time [s]');
    ylabel('Lower pendulum [rad/s]');
    subplot(3, 2, 5);
    plot(t, before_KF(:,3)); hold on;
    plot(t, after_KF(:,3)); grid on;
    xlabel('Time [s]');
    ylabel('Upper pendulum [rad]');
    subplot(3, 2, 6);
    plot(t, before_KF(:,6)); hold on;
    plot(t, after_KF(:,6)); grid on;
    xlabel('Time [s]');
    ylabel('Upper pendulum [rad/s]');
end


